Use programmable action buttons to automate routine actions within and outside [Fibery](https://fibery.io/).

![Close Task and all Subtasks button](close-task-and-all-subtasks/close-task-and-all-subtasks.gif)

### Getting started

1. Learn how to create an action button in our [guide](https://help.fibery.io/en/articles/3369412-action-buttons).

2. Discover what input to expect and how to interact with API on our [dev portal](https://api.fibery.io/#action-buttons).

3. Take a look at the examples in this repo — maybe, someone has already done something similar to your use case.

4. Once you're done, consider contributing the script to this repo — especially, if it's unlike the existing ones.
Your fellow creators would appreciate :)
