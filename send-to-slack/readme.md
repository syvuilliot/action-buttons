![send to slack demo](send-to-slack.gif)

### Fields
* **[Assignments]** 
* **[Rich Text]** Summary
* **[Text]** Slack Member ID on User

### Config
1. Replace Slack webhook URL with yours
2. Adapt the message to your use case 